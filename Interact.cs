﻿using UnityEngine;
using System.Collections;

public class Interact : MonoBehaviour {
	public Transform Playerpos;
	public float xOffset;
	public float yOffset;
	public bool Opened = false;
	protected Transform Object;
	public string Item;
	public float ItemNum;
	public bool ItemOn;
	public bool MessageOn;
	void ItemPickUp() {
			if (ItemNum == 1) {
				Debug.Log (Item);
			} else {
				Debug.Log ("none");
			}
	}
	void MessagePopUp(){
		Debug.Log ("Message");

	}
	void Start () {
		Object = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Opened == false && ItemOn == true) {
			Debug.Log ("close");
			if (Playerpos.transform.position.x <= Object.transform.position.x + xOffset && Playerpos.transform.position.x >= Object.transform.position.x - xOffset && Playerpos.transform.position.y <= Object.transform.position.y + yOffset && Playerpos.transform.position.y >= Object.transform.position.y - yOffset && Input.GetKeyDown (KeyCode.F)) {
				//put outcome here//
				ItemPickUp();
				Opened = true;
			} 
		} 
		if (Opened == false && MessageOn == true) {
			Debug.Log ("close");
			if (Playerpos.transform.position.x <= Object.transform.position.x + xOffset && Playerpos.transform.position.x >= Object.transform.position.x - xOffset && Playerpos.transform.position.y <= Object.transform.position.y + yOffset && Playerpos.transform.position.y >= Object.transform.position.y - yOffset && Input.GetKeyDown (KeyCode.F)) {
				//put outcome here//
				MessagePopUp();
				Opened = true;
			} 

		} 
	}
}


